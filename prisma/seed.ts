import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

async function main() {
    const user = await prisma.user.create({
        data: {
            name: 'John Doe',
            email: 'john.doe@gmail.com',
            avatarUrl: 'https://github.com/rickscombr.png'
        }
    })

    const pool = await prisma.pool.create({
        data: {
            ownerId: user.id,
            title: 'Example Pool',
            code: 'COPA01',

            participants: {
                create: {
                    userId: user.id
                }
            }
        }
    })

    await prisma.game.create({
        data: {
            date: '2022-11-20T14:00:00.201Z',
            firstTeamCountryCode: 'BR',
            secondTeamCountryCode: 'DE',
        }
    })

    await prisma.game.create({
        data: {
            date: '2022-11-22T14:00:00.201Z',
            firstTeamCountryCode: 'BR',
            secondTeamCountryCode: 'AR',

            guesses: {
                create: {
                    firstTeamPoints: 3,
                    secondTeamPoints: 1,
                    
                    participant: {
                        connect: {
                            userId_poolId: {
                                userId: user.id,
                                poolId: pool.id,
                            }
                        }
                    }
                }
            }
        }
    })


}
main()
