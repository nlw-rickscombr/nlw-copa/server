import { FastifyInstance } from 'fastify'
import { prisma } from '../libs/prisma'
import ShortUniqueid from 'short-unique-id'
import { z } from 'zod'
import { authenticate } from '../plugins/authenticate'

export async function poolRoutes(fastify: FastifyInstance) {
    fastify.get('/pools/count', async (request, replay) => {
        const count = await prisma.pool.count()
    
        return replay.status(200).send({ 
            status: 200,
            message: 'OK',
            data: {
                count
            }
        })
    })

    fastify.post('/pools', async (request, replay) => {
        const createPoolBody = z.object({
            title: z.string(),
        })

        const { title } = createPoolBody.parse(request.body)
        
        const generateCode = new ShortUniqueid({ length: 6 })
        const code = String(generateCode()).toUpperCase()

        try {
            await request.jwtVerify()

            await prisma.pool.create({
                data: {
                    title,
                    code,
                    ownerId: request.user.sub,

                    participants: {
                        create: {
                            userId: request.user.sub,
                        }
                    }
                }
            })
        } catch (error) {
            await prisma.pool.create({
                data: {
                    title,
                    code,
                }
            })
        }

        return replay.status(201).send({ 
            status: 201,
            message: 'Poll created successfuly!',
            data: {
                code,
            }
        })
    })

    fastify.post('/pools/join', {
        onRequest: [authenticate],
    },async (request, replay) => {
        const joinPoolBody = z.object({
            code: z.string(),
        })

        const { code  } = joinPoolBody.parse(request.body)

        const pool = await prisma.pool.findUnique({
            where: {
                code,
            },
            include: {
                participants: {
                    where: {
                        userId: request.user.sub
                    }
                }
            }
        })

        if(!pool) {
            return replay.status(404).send({
                statu: 404,
                message: 'Poll not found.',
            })
        }

        if(pool.participants.length > 0){
            return replay.status(400).send({
                status: 400,
                message: 'You already joined this poll.',
            })
        }

        if (!pool.ownerId) {
            await prisma.pool.update({
                where: {
                    id: pool.id,
                },
                data: {
                    ownerId: request.user.sub,
                }
            })
        }

        await prisma.participant.create({
            data: {
                poolId: pool.id,
                userId: request.user.sub,
            }
        })

        return replay.status(201).send({
            status: 201,
            message: 'You joined successfuly!',
        })
    })

    fastify.get('/pools', {
        onRequest: [authenticate],
    }, async (request, replay) => {
        const pools = await prisma.pool.findMany({
            where: {
                participants: {
                    some: {
                        userId: request.user.sub
                    }
                }
            },
            include: {
                _count: {
                    select: {
                        participants: true,
                    }
                },
                participants: {
                    select: {
                        id: true,

                        user: {
                            select: {
                                avatarUrl: true,
                            },
                        },
                    },
                    take: 4,
                },
                owner: {
                    select: {
                        id: true,
                        name: true
                    }
                }
            }
        })

        return replay.status(200).send({ 
            status: 200,
            message: 'List of Polls',
            data: {
                pools
            }
        })
    })

    fastify.get('/pools/:id', {
        onRequest: [authenticate],
    }, async (request, replay) => {
        const getPoolParams = z.object({
            id: z.string(),
        })

        const { id } = getPoolParams.parse(request.params)

        const pool = await prisma.pool.findUnique({
            where: {
                id,
            },
            include: {
                _count: {
                    select: {
                        participants: true,
                    }
                },
                participants: {
                    select: {
                        id: true,

                        user: {
                            select: {
                                avatarUrl: true,
                            },
                        },
                    },
                    take: 4,
                },
                owner: {
                    select: {
                        id: true,
                        name: true
                    }
                }
            }
        })

        // return { pool }
        return replay.status(200).send({ 
            status: 200,
            message: 'Poll found',
            data: {
                pool
            }
        })

    })
}

