import { FastifyInstance } from 'fastify'
import { prisma } from '../libs/prisma'
import { z } from 'zod'
import { authenticate } from '../plugins/authenticate'

export async function guessRoutes(fastify: FastifyInstance) {
    fastify.get('/guesses/count', async () => {
        const count = await prisma.guess.count()

        return { count }
    })

    fastify.post('/pools/:poolId/games/:gameId/guesses', {
        onRequest: [authenticate],
    }, async (request, replay) => {
        const createGuessParams = z.object({
            poolId: z.string(),
            gameId: z.string(),
        })

        const { poolId, gameId } = createGuessParams.parse(request.params)

        const createGuessBody = z.object({
            firstTeamPoints: z.number(),
            secondTeamPoints: z.number(),
        })

        const { firstTeamPoints, secondTeamPoints } = createGuessBody.parse(request.body)

        const participant = await prisma.participant.findUnique({
            where: {
                userId_poolId: {
                    poolId,
                    userId: request.user.sub
                }
            }
        })
        if(!participant) {
            return replay.status(400).send({
                message: "Your're not allowed to create a guess inside this pool."
            })
        }
        
        const game = await prisma.game.findUnique({
            where: {
                id: gameId
            }
        })
        if(!game) {
            return replay.status(404).send({
                message: "Game not found."
            })
        }
        if(game.date < new Date()) {
            return replay.status(403).send({
                message: "You cannot send guesses after the game date."
            })
        }

        const guess = await prisma.guess.findUnique({
            where: {
                participantId_gameId: {
                    participantId: participant.id,
                    gameId
                }
            }
        })
        if(guess) {
            // SE JA TEM PALPITE MAS AINDA NÃO PASSOU A DATA DO JOGO, ATUALIZA
            await prisma.guess.update({
                where: {
                    id: guess.id
                },
                data: {
                    firstTeamPoints,
                    secondTeamPoints
                }
            })
            return replay.status(200).send({
                message: "You guess updated successfuly!"
            })
            // RETORNANR QUE USUÁRIO SÓ PODE REALIZAR UM PALPITE
            // return replay.status(400).send({
            //     message: "You already sent a guess to this game on this poll.",
            //     guess
            // })
        }

        await prisma.guess.create({
            data: {
                gameId,
                participantId: participant.id,
                firstTeamPoints,
                secondTeamPoints
            }
        })

        return replay.status(201).send({
            message: 'Guess registred successfuly!'
        })
    })
}