import Fastify from 'fastify'
import cors from '@fastify/cors'
import jwt from '@fastify/jwt'

import { authRoutes } from './routes/auth'
import { poolRoutes } from './routes/pool'
import { userRoutes } from './routes/user'
import { guessRoutes } from './routes/guess'
import { gameRoutes } from './routes/game'

// Run the server!
async function start() {
    const fastify = Fastify({
        logger: true,
    })

    await fastify.register(cors, {  
        origin: true,
    })

    // Alterar secret para variável de ambiente 
    await fastify.register(jwt, {  
        secret: 'NLWCOPA-RICKS-2022',
    })

    await fastify.register(authRoutes)
    await fastify.register(poolRoutes)
    await fastify.register(userRoutes)
    await fastify.register(guessRoutes)
    await fastify.register(gameRoutes)


    await fastify.listen({ port: 3030, host: '0.0.0.0' })
}

start()